﻿using Microsoft.AspNetCore.Mvc;
using MrChess.Server;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MrChess.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MoveController : ControllerBase
    {
        [HttpGet("{id}")]
        public MoveDTO Get(string id)
        {
            var m = ServerController.I.GetMove(id);
            return m == null ? 
                new MoveDTO() { PlayerId = MoveDTO.NoMove } :
                new MoveDTO()
                {
                    FromX = m.FromX,
                    FromY = m.FromY,
                    ToX = m.ToX,
                    ToY = m.ToY,
                };
        }

        [HttpPost]
        public void Post(MoveDTO move)
        {
            ServerController.I.Move(move);
        }
    }
}
