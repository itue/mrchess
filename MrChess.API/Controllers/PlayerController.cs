﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MrChess.Server
{
    [Route("[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        [HttpGet("{login}")]
        public Player Get(string login)
        {
            return ServerController.I.GetAddPlayer(login, "1");
        }

        // GET: api/<GameController>
        [HttpGet]
        public IEnumerable<Player> Get()
        {
            return ServerController.I.Players;
        }
    }
}
