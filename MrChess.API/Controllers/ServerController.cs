﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MrChess.Server
{
    public class ServerController
    {
        public static readonly ServerController I = new ServerController();

        private ServerController() { }

        public List<Player> Players { get; set; } = new List<Player>();
        public List<Match> ActiveMathces { get; set; } = new List<Match>();

        internal Move GetMove(string id)
        {
            // если матчей много, лучше использовать словарь по id матча
            var m = ActiveMathces.Find(x => x.Player1.Id == id && x.Player2.Id == id);
            return m?.Moves.LastOrDefault();
        }

        internal Player GetAddPlayer(string login, string pass)
        {
            var p = Players.Find(x => x.Login == login);
            if (p != null)
            {
                if (p.Password == pass) return p;
                throw new Exception("Wrong password");
            }
            p = new Player()
            {
                Login = login,
                Password = pass,
                Id = Guid.NewGuid().ToString("N"),
            };
            Players.Add(p);
            return p;
        }

        internal void Move(MoveDTO move)
        {
            var match = ActiveMathces.Find(x =>
                x.Player1.Id == move.PlayerId ||
                x.Player2.Id == move.PlayerId);
            if (match == null)
                throw new Exception("No match");
            match.TryMove(move);
        }

        internal async Task<Match> StartMatch(string id)
        {
            var p = Players.Find(x => x.Id == id);
            if (p == null) throw new Exception("No player " + id);
            return await Task.Run<Match>(async () =>
            {
                p.State = EState.InSearch;
                while (true)
                {
                    var free = Players.Find(x => x.State == EState.InSearch);
                    if (free != null)
                    {
                        p.State = EState.Game;
                        var m = new Match(p, free, 8);
                        ActiveMathces.RemoveAll(x => x.Player1.Id == id);
                        ActiveMathces.Add(m);
                        return m;
                    }
                    await Task.Delay(1);
                }
            });
        }
    }
}
