﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MrChess.Server
{
    [Route("[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        // API
        // 1. Player/login - создание аккаунта или логин
        // 2. Player - список игроков (для тестов)
        // 3. Game/playerId - запуск матча, id игрока
        // 4. Game/MoveDTO - ход
        // 5. Game - список матчей

        // GET api/<GameController>/5
        [HttpGet("{id}")]
        public async Task<MatchDTO> Get(string id)
        {
            var m = await ServerController.I.StartMatch(id);
            if (m == null) throw new Exception();
            return new MatchDTO(m);
        }

        [HttpPost]
        public void Post([FromBody] MoveDTO move)
        {
            ServerController.I.Move(move);
        }

        [HttpGet]
        public List<Match> Get()
        {
            return ServerController.I.ActiveMathces;
        }

        //// POST api/<GameController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/<GameController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<GameController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
