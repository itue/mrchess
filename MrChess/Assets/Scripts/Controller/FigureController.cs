﻿using DG.Tweening;
using UnityEngine;

public class FigureController : MonoBehaviour, IClick
{
    public EFigure Id;
    internal IFigure Figure;
    private Board Board;
    public Material Black;

    public static FigureController SelectedFigure;

    internal static void TryMove(int x, int y, Board board)
    {
        if (SelectedFigure)
        {
            var figure = SelectedFigure.Figure;
            if (board.TryMove(figure, x, y))
                Unselect();
        }
    }

    //private void Start()
    //{
    //    Events.OnClick += OnClick;
    //}

    //private void OnDestroy()
    //{
    //    Events.OnClick -= OnClick;
    //}

    public void OnClick()
    {
        if (Board.WhiteMove != Figure.IsWhite)
        {
            TryMove(Figure.X, Figure.Y, Board);
            return;
        }
        Unselect();
        transform.DOScale(1.2f, 0.4f);
        //    .OnComplete(() => transform.DOScale(1f, 0.3f));
        SelectedFigure = this;
    }

    public static void Unselect()
    {
        if (SelectedFigure)
            SelectedFigure.transform.DOScale(1f, 0.3f);
    }

    internal void Init(IFigure f, Board board)
    {
        Figure = f;
        Board = board;
        var rr = GetComponentsInChildren<Renderer>();
        foreach (var r in rr)
            if (!f.IsWhite)
                r.material = Black;
        Move();
    }

    internal void Move()
    {
        transform.localPosition = new Vector3(Figure.X, 0, Figure.Y);
    }
}
