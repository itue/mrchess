﻿using DG.Tweening;
using UnityEngine;

public class CellController : MonoBehaviour, IClick
{
    public int X, Y;
    public Board Board;

    public void OnClick()
    {
        transform.DOScale(1.1f, 0.4f)
            .OnComplete(() => transform.DOScale(1f, 0.3f));
        FigureController.TryMove(X, Y, Board);
    }
}
