﻿using UnityEngine;

namespace Assets.Scripts.Controller
{
    internal static class PrefabExt
    {
        public static FigureController GetPrefab(this EFigure e) =>
            Resources.Load<FigureController>(e.ToString());
    }
}
