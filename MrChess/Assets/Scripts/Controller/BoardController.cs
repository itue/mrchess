using Assets.Scripts.Controller;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    public Transform Dno;
    public Transform Root;
    public Transform CellsRoot;
    public CellController CellPrefabW, CellPrefabB;
    Board Board;

    float _nextSync;
    float SyncInterval = 0.5f;
    //public List<FigureController> FigurePrefabs;

    void Start()
    {
        var match = MatchController.I.Match; //new Match(new Player(), new Player());
        Board = match.Board;

        for (int i = 0; i < Board.W; i++)
            for (int j = 0; j < Board.H; j++)
            {
                var cell = Instantiate((i + j) % 2 == 0 ?
                    CellPrefabW : CellPrefabB, CellsRoot);
                cell.transform.localPosition = new Vector3(i, 0, j);
                cell.Board = Board;
                cell.X = i;
                cell.Y = j;
            }

        Root.localPosition = new Vector3(-Board.W / 2 + 0.5f, 0, -Board.H / 2 + 0.5f);
        CellsRoot.localPosition = new Vector3(-Board.W / 2 + 0.5f, -0.5f, -Board.H / 2 + 0.5f);
        /*Dno.localScale = new Vector3(Board.W - 0.3f, 1, Board.H - 0.3f);*/
        ShowFigures();
        Events.OnFigureMove += OnFigureMove;
    }

    private void OnDestroy()
    {
        Events.OnFigureMove -= OnFigureMove;
    }

    private void OnFigureMove(IFigure figure, int oldx, int oldy)
    {
        if (MatchController.IsMultiplayer)
        {
            var m = MatchController.I.Match;
            ServerClient.I.SendMove(new MoveDTO()
            {
                FromX = oldx,
                FromY = oldy,
                ToX = figure.X,
                ToY = figure.Y,
                PlayerId = figure.IsWhite ? m.Player1.Id : m.Player2.Id,
            });
        }
        ShowFigures();
    }

    private void ShowFigures()
    {
        //Debug.Log(Board.Figures().Count);
        Clear();
        foreach (var f in Board.Figures)
            ShowFigure(f);
    }

    private void ShowFigure(IFigure f)
    {
        //var prefab = FigurePrefabs.Find(x => x.Id == f.Id);
        var prefab = f.Id.GetPrefab();
        //Debug.Log("������ " + f.Id + " " + prefab);
        if (!prefab) prefab = EFigure.Peshka.GetPrefab();
        var fig = Instantiate(prefab, Root);
        fig.Init(f, Board);
    }

    private void Clear()
    {
        foreach (Transform f in Root)
            Destroy(f.gameObject);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Events.OnClick?.Invoke();
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit))
            {
                hit.transform.GetComponent<IClick>()?.OnClick();
            }
        }

        if (MatchController.IsMultiplayer && _nextSync < Time.time)
        {
            _nextSync = Time.time + SyncInterval;
            var m = ServerClient.I.SendGetMove(MatchController.I.Match.Player1.Id);
            Debug.Log($"Move {m}");
            if (m == null)
                Debug.LogError("MoveDTO == null");
            else if (!m.IsNoMove())
            {
                var f = Board.Cells[m.FromX, m.FromY];
                if (f != null)
                {
                    f.MoveToForce(Board, m);
                    //MatchController.I.Match.TryMove(m);
                    Debug.Log($"Move OK {m}");
                    ShowFigures();
                    Board.WhiteMove = !Board.WhiteMove;
                }
            }
        }
    }
}

public interface IClick
{
    void OnClick();
}