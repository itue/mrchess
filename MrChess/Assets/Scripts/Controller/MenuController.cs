using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Button Start8, Start12, Start16, Account, Rating;

    void Start()
    {
        Start8.onClick.AddListener(() => StartMatch(8));
        Start12.onClick.AddListener(() => StartMatch(12));
        Start16.onClick.AddListener(() => StartMatch(16));
        Account.onClick.AddListener(StartMatchMultiplayer);
    }

    private void StartMatchMultiplayer()
    {
        var p = ServerClient.I.GetPlayer("123", "123");
        Debug.Log("Test 1 ok: Login=" + p.Login);
        var m = ServerClient.I.GetMatch(p.Id);
        Debug.Log("Test 2 ok: Figure[1, 3]=" + m.Board.Cells[1, 3].Id);
        MatchController.I.Match = m;
        MatchController.IsMultiplayer = true;
        SceneManager.LoadSceneAsync("MatchScene");
    } 
    
    private void StartMatch(int size)
    {
        MatchController.I.Match = new Match(
            new Player(), new Player(), size);
        SceneManager.LoadSceneAsync("MatchScene");
        //DontDestroyOnLoad(this);
    }
}
