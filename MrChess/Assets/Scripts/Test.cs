﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

public class Test
{
    [MenuItem("Tools/Loacl")]
    public static void Local() => ServerClient.I.SetServer("http://localhost:13031/");

    [MenuItem("Tools/Global")]
    public static void Global() =>
        ServerClient.I.SetServer("https://distracted-benz.37-140-192-97.plesk.page/");

    //[MenuItem("Tools/Test")]
    public static void DoTest()
    {
        Debug.Log("Test begin");

        var p = ServerClient.I.GetPlayer("123", "123");
        Assert.IsNotNull(p);
        Assert.AreEqual("123", p.Login);
        Debug.Log("Test 1 ok: Login=" + p.Login);

        var m = ServerClient.I.GetMatch(p.Id);
        Assert.IsNotNull(m);
        Assert.AreEqual(EFigure.Peshka, m.Board.Cells[1, 3].Id);
        Debug.Log("Test 2 ok: Figure[1, 3]=" + m.Board.Cells[1, 3].Id);

        //var dto = new MoveDTO()
        //{
        //    PlayerId = p.Id,
        //    FromX = 1,
        //    FromY = 3,
        //    ToX = 2,
        //    ToY = 3,
        //};
        //ServerClient.I.SendMoveAsync(dto);
        //Debug.Log("Test 3 ok");
    }
}
#endif