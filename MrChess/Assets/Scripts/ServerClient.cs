﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using UnityEngine;

public class ServerClient
{
    public static readonly ServerClient I = new ServerClient();

    //string Uri = "http://localhost:13031/";
    string Uri = "https://distracted-benz.37-140-192-97.plesk.page/";
    WebClient client = new WebClient();

    public void SetServer(string uri)
    {
        Uri = uri;
    }

    ~ServerClient()
    {
        client.Dispose();
    }

    public T Get<T>(string path)
    {
        var res = client.DownloadString(Uri + path);
        //var playerS = res.Content.ReadAsStringAsync().Result;
        Debug.Log(res);
        var player = JsonConvert.DeserializeObject<T>(res);
        return player;
    }

    public void Post<T>(string path, T data)
    {
        var d = JsonConvert.SerializeObject(data);
        client.Headers[HttpRequestHeader.ContentType] = "application/json";
        //var content = new StringContent(d, Encoding.UTF8, "application/json");
        var r = client.UploadString(Uri + path, "POST", d);
            //.PostAsync(Uri + path, content).Result;
    }

    public Player GetPlayer(string login, string pass) =>
        Get<Player>($"Player/{login}");
    public Match GetMatch(string playerId)
    {
        var m = Get<MatchDTO>($"Game/{playerId}");
        return m.Load();
    }

    public void SendMove(MoveDTO move) =>
        Post($"Move", move);

    public MoveDTO SendGetMove(string id) =>
        Get<MoveDTO>($"Move/" + id);
}
