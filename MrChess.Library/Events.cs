﻿using System;

public static class Events
{
    public delegate void OnFigureMoveDelegate(IFigure figure, int x, int y);
 
    public static Action OnClick;
    public static Action<IFigure> OnFigureClick;
    public static OnFigureMoveDelegate OnFigureMove;
}
