﻿public class MatchDTO
{
    public Player Player1 { get; set; }
    public Player Player2 { get; set; }
    public BoardDTO Board { get; set; }

    public MatchDTO() { }
    public MatchDTO(Match m)
    {
        Player1 = m.Player1;
        Player2 = m.Player2;
        Board = new BoardDTO(m.Board);
    }

    public Match Load()
    {
        return new Match()
        {
            Player1 = Player1,
            Player2 = Player2,
            Board = Board.Load(),
        };
    }
}
