﻿public class FigureDTO
{
    public EFigure Id { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
    public bool IsWhite { get; set; }

    public FigureDTO() { }
    public FigureDTO(IFigure f)
    {
        Id = f.Id;
        X = f.X;
        Y = f.Y;
        IsWhite = f.IsWhite;
    }

    public IFigure Load()
    {
        var f = GetFigure();
        f.X = X;
        f.Y = Y;
        f.IsWhite = IsWhite;
        return f;
    }

    Figure GetFigure()
    {
        switch (Id)
        {
            case EFigure.Korol: return new FigureKorol();
            default: return new FigurePeshka();
        }
    }
}
