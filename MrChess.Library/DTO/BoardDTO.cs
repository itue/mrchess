﻿using System.Collections.Generic;
using System.Linq;

public class BoardDTO
{
    public bool WhiteMove { get; set; }
    public int W { get; set; }
    public int H { get; set; }
    public List<FigureDTO> Figures { get; set; }

    public BoardDTO() { }
    public BoardDTO(Board board)
    {
        H = board.H;
        W = board.W;
        WhiteMove = board.WhiteMove;
        Figures = board.Figures.Select(x => new FigureDTO(x)).ToList();
    }

    public Board Load()
    {
        return new Board(W, H)
        {
            WhiteMove = WhiteMove,
            Figures = Figures.Select(x => x.Load()).ToList(),
        };
    }
}
