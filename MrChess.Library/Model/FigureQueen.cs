﻿using System;

public class FigureFerz : Figure
{
    public override EFigure Id => EFigure.Ferz;
    public override int Cost => 1;

    public override bool CanMoveTo(int x, int y, Board board)
    {
        return (X != x) || (Y != y) || (x == y && X != x && Y != y) ||
            (x == Math.Abs(y) && X != x && Y != y) || (Math.Abs(x) == y && X != x && Y != y);
    }
}