﻿
public class FigureKorol : Figure
{
    public override EFigure Id => EFigure.Korol;
    public override int Cost => 5;

    public override bool CanMoveTo(int x, int y, Board board)
    {
        return x >= X - 1 && x <= X + 1 && y >= Y - 1 && y <= Y + 1 && (x != X || y != Y);
    }
}