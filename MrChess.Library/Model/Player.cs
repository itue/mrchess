﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

public enum EState { No, Offline, Online, InSearch, Game }

public class Player
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public EState State { get; set; }

    //public List<Match> Matches { get; set; }
}
