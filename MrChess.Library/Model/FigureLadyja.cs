﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MrChess.Library.Model
{
    public class FigureLadyja : Figure
    {
        public override EFigure Id => EFigure.Ladyja;
        public override int Cost => 5;

        public override bool CanMoveTo(int x, int y, Board board)
        {
            bool res = true;
            if (x > X && y == Y)
            {
                for (int i = X + 1; i == x - 1; i++)
                {
                    if (board.Cells[i, Y] != null) res = false;
                }
                if (board.Cells[x, Y] != null && this.IsWhite == board.Cells[x, Y].IsWhite) res = false;
            }
            else if (x < X && y == Y)
            {
                for (int i = X - 1; i == x + 1; i--)
                {
                    if (board.Cells[i, Y] != null) res = false;
                }
                if (board.Cells[x, Y] != null && this.IsWhite == board.Cells[x, Y].IsWhite) res = false;
            }
            else if (x == X && y > Y)
            {
                for (int i = Y + 1; i == y - 1; i++)
                {
                    if (board.Cells[X, i] != null) res = false;
                }
                if (board.Cells[X, y] != null && this.IsWhite == board.Cells[X, y].IsWhite) res = false;
            }
            else if (x == X && y < Y)
            {
                for (int i = Y - 1; i == y + 1; i--)
                {
                    if (board.Cells[X, i] != null) res = false;
                }
                if (board.Cells[X, y] != null && this.IsWhite == board.Cells[X, y].IsWhite) res = false;
            }

            return res;
        }
    }
}