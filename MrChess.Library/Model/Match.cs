﻿using System.Collections.Generic;

public class Match
{
    public Player Player1 { get; internal set; }
    public Player Player2 { get; internal set; }
    public List<Move> Moves { get; }
    public Board Board { get; set; }

    internal Match() { }
    public Match(Player player1, Player player2, int size = 8)
    {
        Player1 = player1;
        Player2 = player2;
        Moves = new List<Move>();
        Board = new Board(size, size);
    }

    public bool TryMove(MoveDTO move)
    {
        var figure = Board.Cells[move.FromX, move.FromY];
        if (Board.TryMove(figure, move.ToX, move.ToY))
        {
            if (Moves != null)
                Moves.Add(new Move()
                {
                    Figure = figure,
                    FromX = move.FromX,
                    FromY = move.FromY,
                    ToX = move.ToX,
                    ToY = move.ToY
                });
            return true;
        }
        return false;
    }
}
