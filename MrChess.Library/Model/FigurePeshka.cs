﻿
public class FigurePeshka : Figure
{
    public override EFigure Id => EFigure.Peshka;
    public override int Cost => 1;

    public override bool CanMoveTo(int x, int y, Board board)
    {
        return x != X || y != Y;
    }
}