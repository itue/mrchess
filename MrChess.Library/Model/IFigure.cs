﻿public enum EFigure
{
    No = 0,
    Peshka,
    Slon,
    Konj,
    Ladyja,
    Ferz,
    Korol,
}

public interface IFigure
{
    EFigure Id { get; }
    int X { get; }
    int Y { get; }
    int Cost { get; }
    bool IsWhite { get; }
    bool CanMoveTo(int x, int y, Board board);
    bool MoveTo(int x, int y, Board board);
    void MoveToForce(Board board, MoveDTO m);
}
