using System.Collections.Generic;
using System.Linq;

public class Board
{
    public bool WhiteMove { get; set; } = true;
    public int W { get; }
    public int H { get; }

    public IFigure[,] Cells { get; internal set; }
    public List<IFigure> Figures
    {
        get => Cells.OfType<IFigure>().ToList();
        set
        {
            Cells = new IFigure[W, H];
            foreach (var cell in value)
                Cells[cell.X, cell.Y] = cell;
        }
    }

    public Board(int w, int h)
    {
        W = w;
        H = h;
        Cells = new IFigure[W, H];
        AddDefaultFigures();
    }

    public bool TryMove(IFigure figure, int x, int y)
    {
        return figure.CanMoveTo(x, y, this) ?
            figure.MoveTo(x, y, this) : false;
    }

    void AddDefaultFigures()
    {
        for (int i = 0; i < H; i++)
        {
            Add<FigurePeshka>(0, i);
            Add<FigurePeshka>(1, i);
        }
        Add<FigureSlon>(0, 2);
        Add<FigureKorol>(0, 3);
        Add<FigureFerz>(0, 4);
        Add<FigureSlon>(0, 5);

        void Add<T>(int col, int y) where T : Figure, new()
        {
            new T() { IsWhite = true }.MoveTo(col, y, this);
            new T().MoveTo(W - col - 1, y, this);
        }
    }
}
