﻿public class Move
{
    public int FromX { get; set; }
    public int FromY { get; set; }
    public int ToX { get; set; }
    public int ToY { get; set; }
    public IFigure Figure { get; set; }
}

public class MoveDTO
{
    public const string NoMove = "-";
    public bool IsNoMove() => PlayerId == NoMove;

    public string PlayerId { get; set; }
    public int FromX { get; set; }
    public int FromY { get; set; }
    public int ToX { get; set; }
    public int ToY { get; set; }

    public override string ToString() => $"{PlayerId} {FromX},{FromY} -> {ToX},{ToY}";
}