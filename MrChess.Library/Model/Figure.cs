﻿public abstract class Figure : IFigure
{
    public abstract EFigure Id { get; }
    public abstract int Cost { get; }
    public int X { get; internal set; }
    public int Y { get; internal set; }
    public bool IsWhite { get; set; }

    public abstract bool CanMoveTo(int x, int y, Board board);

    public bool MoveTo(int x, int y, Board board)
    {
        board.Cells[X, Y] = null;
        board.Cells[x, y] = this;
        board.WhiteMove = !board.WhiteMove;
        var oldx = X;
        var oldy = Y;
        X = x; Y = y;
        Events.OnFigureMove?.Invoke(this, oldx, oldy);
        return true;
    }

    public void MoveToForce(Board board, MoveDTO m)
    {
        board.Cells[m.FromX, m.FromY] = null;
        board.Cells[m.ToX, m.ToY] = this;
        X = m.ToX;
        Y = m.ToY;
    }
}
