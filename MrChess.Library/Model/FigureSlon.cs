using System;

public class FigureSlon : Figure
{
    public override EFigure Id => EFigure.Slon;

    public override int Cost => 3;
    private bool check_inv_diagonal(int x, int y, Board board, int k = -1)
    {
        /*
          �������� �� ���������� ����� �������
         */
        for (int i = X, j = Y; i > X + 1 && j > Y + 1; i += k, j += k)
        {
            if (board.Cells[i, j] != null) { return false; }
        }
        return true;
    }
    private bool check_diagonal(int x, int y, Board board, int k = 1)
    {
        /*
          �������� �� ���������� ����� �������
        */
        for (int i = X, j = Y; i < X - 1 && j < Y - 1; i += k, j += k)
        {
            if (board.Cells[i, j] != null) { return false; }
        }
        return true;
    }
    private bool check_sub_inv_diagonal(int x, int y, Board board)
    {
        /*
          �������� �� ���������� ����� �������
         */
        for (int i = X, j = Y; i > X + 1 && j < Y - 1; i -= 1, j += 1)
        {
            if (board.Cells[i, j] != null) { return false; }
        }
        return true;
    }
    private bool check_sub_diagonal(int x, int y, Board board, int k = 1)
    {
        /*
          �������� �� ���������� ����� �������
        */
        for (int i = X, j = Y; i < X - 1 && j > Y + 1; i += k, j -= k)
        {
            if (board.Cells[i, j] != null) { return false; }
        }
        return true;
    }
    public override bool CanMoveTo(int x, int y, Board board)
    {
        /* 

         �������� ����������� ���� ������. 
         x � y - ���������� ���� ������. 
         board - �����, �� ������� ������������ ��� (������� n*n)

         */

        if (((X + Y == x + y) || Math.Sqrt(X - Y) == Math.Sqrt(x - y))
            && (IsWhite != board.Cells[x, y].IsWhite || board.Cells[x, y] == null))
        {
            if (X > x && Y > y) return check_diagonal(x, y, board);
            if (X > x && Y < y) return check_sub_diagonal(x, y, board);
            if (X < x && Y < y) return check_inv_diagonal(x, y, board);
            if (X < x && Y > y) return check_sub_inv_diagonal(x, y, board);
        }

        return false;
    }
}
