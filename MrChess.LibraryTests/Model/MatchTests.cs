﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass()]
    public class MatchTests
    {
        [TestMethod()]
        public void MatchTest()
        {
            var p1 = new Player();
            var p2 = new Player();
            var n = 8;

            var m = new Match(p1, p2, n);

            Assert.AreEqual(n * n, m.Board.Cells.Length);
            Assert.AreEqual(p1, m.Player1);
            Assert.AreEqual(p2, m.Player2);
        }

        [TestMethod()]
        public void MoveTest()
        {
            var p1 = new Player();
            var p2 = new Player();
            var n = 8;

            var m = new Match(p1, p2, n);
            var move = new MoveDTO() { FromX = 1, ToX = 2, FromY = 3, ToY = 3 };
            Assert.AreEqual(EFigure.Peshka, m.Board.Cells[move.FromX, move.FromY].Id);

            Assert.IsTrue(m.TryMove(move));

            Assert.AreEqual(null, m.Board.Cells[move.FromX, move.FromY]);
            Assert.AreEqual(EFigure.Peshka, m.Board.Cells[2, 3].Id);
        }

        [TestMethod()]
        public void ServerSendMoveTest()
        {
            var move = new MoveDTO() { FromX = 1, FromY = 3, ToX = 2, ToY = 3, PlayerId = "" };
            ServerClient.I.SendMove(move);
        }

        [TestMethod()]
        public void ServerMoveTest()
        {
            var p = ServerClient.I.GetPlayer("123", "123");
            Assert.IsNotNull(p);
            Assert.AreEqual("123", p.Login);

            var m = ServerClient.I.GetMatch(p.Id);
            Assert.IsNotNull(m);
            var figure = m.Board.Cells[1, 3];
            var figureB = m.Board.Cells[6, 3];
            Assert.AreEqual(EFigure.Peshka, figure.Id);
            Assert.AreEqual(true, figure.IsWhite);
            Assert.AreEqual(false, figureB.IsWhite);

            var noMove = ServerClient.I.SendGetMove(p.Id);
            System.Console.WriteLine(JsonConvert.SerializeObject(noMove));

            var move = new MoveDTO() { FromX = 1, FromY = 3, ToX = 2, ToY = 3, PlayerId = p.Id };
            ServerClient.I.SendMove(move);

            var getMove = ServerClient.I.SendGetMove(p.Id);
            System.Console.WriteLine(JsonConvert.SerializeObject(getMove));
            Assert.AreEqual(getMove.FromX, 1);
            Assert.AreEqual(getMove.ToX, 2);
        }
    }
}